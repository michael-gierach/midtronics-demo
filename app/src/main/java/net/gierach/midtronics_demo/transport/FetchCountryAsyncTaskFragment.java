package net.gierach.midtronics_demo.transport;

import android.os.AsyncTask;
import android.util.Log;

import androidx.fragment.app.Fragment;

import net.gierach.midtronics_demo.model.CountryDao;
import net.gierach.midtronics_demo.model.CountryRecord;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class FetchCountryAsyncTaskFragment extends Fragment {

    public static final String TAG = "FetchCountryFrag";

    public interface Callback {
        void onCountryFetched(FetchCountryAsyncTaskFragment fetchCountryFragment, String countryName, CountryRecord result);
    }

    private String mCountryName;
    private Callback mCallback;
    private CountryRecord mResult;
    private boolean mComplete;
    private FetchCountryAsyncTask mTask;

    public boolean isComplete() {
        return mComplete;
    }

    public CountryRecord getResult() {
        return mResult;
    }

    public void fetchCountry(Callback callback, String countryName) {
        mCallback = callback;
        mCountryName = countryName;
        if (mTask == null) {
            mTask = new FetchCountryAsyncTask();
            mTask.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
        }
    }

    public void unregisterCallback(Callback callback) {
        if (mCallback == callback) {
            mCallback = null;
        }
    }


    private class FetchCountryAsyncTask extends AsyncTask<Void, Void, CountryRecord> {
        @Override
        protected CountryRecord doInBackground(Void... voids) {

            ICountryRestApi restApi = CountryRestApiFactory.getCountryRestApi();
            RestCountry found = null;
            List<RestCountry> resultList = null;
            Call<List<RestCountry>> call = restApi.getCountryByName(mCountryName);
            try {
                Response<List<RestCountry>> response = call.execute();
                if (response.isSuccessful()) {
                    resultList = response.body();
                    Log.d(TAG, "::doInBackground Success");
                } else {
                    Log.d(TAG, "::doInBackground Failure responseCode:" + response.code() + " responseMessage:" + response.message());
                }
            } catch (IOException ioe) {
                Log.e(TAG, "::doInBackground Error getting country by name.");
            }

            if (resultList != null && resultList.size() > 0) {
                for (RestCountry country : resultList) {
                    if (country.name != null && mCountryName.equalsIgnoreCase(country.name.common)) {
                        found = country;
                        break;
                    }
                }
                //We didn't find an exact match in the results, so just return the first one.
                if (found == null) {
                    found = resultList.get(0);
                }
            }

            if (found != null) {
                return new CountryRecord(mCountryName, found);
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(CountryRecord countryRecord) {
            mComplete = true;
            mResult = countryRecord;
            if (countryRecord != null) {
                CountryDao.getInstance().putCountryRecord(countryRecord);
            }
            if (mCallback != null) {
                mCallback.onCountryFetched(FetchCountryAsyncTaskFragment.this, mCountryName, countryRecord);
            }
        }

        @Override
        protected void onCancelled() {
            mComplete = true;
            if (mCallback != null) {
                mCallback.onCountryFetched(FetchCountryAsyncTaskFragment.this, mCountryName, null);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mTask != null && !mComplete) {
            mTask.cancel(true);
        }
    }
}
