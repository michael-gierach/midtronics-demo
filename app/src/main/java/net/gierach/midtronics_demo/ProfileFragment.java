package net.gierach.midtronics_demo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ProfileFragment extends Fragment {

    private WebView mWebView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mWebView = new WebView(getContext());

        mWebView.loadUrl("file:///android_res/raw/resume.html");

        return mWebView;
    }

    @Override
    public void onDestroyView() {
        mWebView.destroy();
        mWebView = null;

        super.onDestroyView();
    }
}
