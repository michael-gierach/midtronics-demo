package net.gierach.midtronics_demo;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CountryListFragment extends Fragment {
    private class CountryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private String mCountry;
        private final TextView mTextView;

        public CountryViewHolder(@NonNull View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.country_list_item_text);
            mTextView.setOnClickListener(this);
        }

        public void setCountryText(String country) {
            mTextView.setText(country);
            mCountry = country;
        }

        @Override
        public void onClick(View v) {
            if (!TextUtils.isEmpty(mCountry) && mCanDispatchClick) {
                mCanDispatchClick = false;
                startActivity(CountryDetailActivity.createLaunchIntent(getContext(), mCountry));
            }
        }
    }

    private class CountryAdapter extends RecyclerView.Adapter<CountryViewHolder> {
        private String[] mCountryList;

        public CountryAdapter(String[] countryList) {
            mCountryList = countryList;
        }

        @NonNull
        @Override
        public CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

            return new CountryViewHolder(layoutInflater.inflate(R.layout.country_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull CountryViewHolder holder, int position) {
            holder.setCountryText(mCountryList[position]);
        }

        @Override
        public int getItemCount() {
            return mCountryList.length;
        }

        void setCountryList(String[] countryList) {
            mCountryList = countryList;
            notifyDataSetChanged();
        }
    }

    private static final String SAVE_STATE_SEARCH_QUERY = "SEARCH_QUERY";

    private boolean mCanDispatchClick = false;
    private String mSearchText = "";
    private String[] mOrigCountryList;
    private String[] mCaselessCountryList;
    private CountryAdapter mAdapter;

    private static final int MSG_DO_SEARCH = 1;
    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == MSG_DO_SEARCH) {
                executeSearch();
            }
        }
    };

    public CountryListFragment() {
        super();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.country_list_fragment, container, false);

        mOrigCountryList = getResources().getStringArray(R.array.countries_array);
        mCaselessCountryList = new String[mOrigCountryList.length];
        for (int i = 0; i < mOrigCountryList.length; ++i) {
            mCaselessCountryList[i] = mOrigCountryList[i].toLowerCase();
        }
        RecyclerView recyclerView = view.findViewById(R.id.countries_list);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), getResources().getInteger(R.integer.countries_list_columns)));
        mAdapter = new CountryAdapter(mOrigCountryList);
        if (savedInstanceState != null && savedInstanceState.containsKey(SAVE_STATE_SEARCH_QUERY)) {
            mSearchText = savedInstanceState.getString(SAVE_STATE_SEARCH_QUERY);
            executeSearch();
        } else {
            mSearchText = "";
        }
        recyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.country_list_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.app_bar_search);
        SearchView searchView = (SearchView)menuItem.getActionView();

        if (!TextUtils.isEmpty(mSearchText)) {
            searchView.setQuery(mSearchText, false);
            menuItem.expandActionView();
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mSearchText = query;
                mHandler.removeMessages(MSG_DO_SEARCH);
                executeSearch();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mHandler.removeMessages(MSG_DO_SEARCH);
                mSearchText = newText;

                mHandler.sendEmptyMessageDelayed(MSG_DO_SEARCH, 200);

                return true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        mCanDispatchClick = true;
    }

    private void executeSearch() {
        String[] filteredCountries;
        if (TextUtils.isEmpty(mSearchText)) {
            filteredCountries = mOrigCountryList;
        } else {
            ArrayList<String> tempFilter = new ArrayList<>(mOrigCountryList.length);
            String caselessQuery = mSearchText.toLowerCase();
            for (int i = 0; i < mCaselessCountryList.length; ++i) {
                if (mCaselessCountryList[i].contains(caselessQuery)) {
                    tempFilter.add(mOrigCountryList[i]);
                }
            }
            filteredCountries = tempFilter.toArray(new String[0]);
        }

        mAdapter.setCountryList(filteredCountries);
    }


}

