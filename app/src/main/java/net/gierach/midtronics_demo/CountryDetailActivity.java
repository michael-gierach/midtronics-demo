package net.gierach.midtronics_demo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import net.gierach.midtronics_demo.model.CountryDao;
import net.gierach.midtronics_demo.model.CountryRecord;
import net.gierach.midtronics_demo.transport.FetchCountryAsyncTaskFragment;

import java.text.NumberFormat;

public class CountryDetailActivity extends AppCompatActivity implements FetchCountryAsyncTaskFragment.Callback {

    private static final String EXTRA_COUNTRY_NAME = "COUNTRY_NAME";

    @NonNull
    public static Intent createLaunchIntent(Context context, @NonNull String countryName) {
        Intent intent = new Intent(context, CountryDetailActivity.class);
        intent.putExtra(EXTRA_COUNTRY_NAME, countryName);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        return intent;
    }


    private String mCountryName;

    private View mLoadingLayout;
    private FetchCountryAsyncTaskFragment mFetchCountryFragment;
    private TextView mTitle;
    private ImageView mFlagImage;
    private ImageView mCoatOfArmsImage;
    private DetailsLabelValueLayout mOfficialName;
    private DetailsLabelValueLayout mRegion;
    private DetailsLabelValueLayout mSubregion;
    private DetailsLabelValueLayout mCapitol;
    private DetailsLabelValueLayout mPopulation;
    private DetailsLabelValueLayout mAreaKm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCountryName = getIntent().getStringExtra(EXTRA_COUNTRY_NAME);
        if (TextUtils.isEmpty(mCountryName)) {
            finish();
            return;
        }

        setContentView(R.layout.country_detail_activity);
        setTitle(mCountryName);

        mLoadingLayout = findViewById(R.id.loading_layout);
        mTitle = findViewById(R.id.country_name);
        mFlagImage = findViewById(R.id.flag_image);
        mCoatOfArmsImage = findViewById(R.id.coat_of_arms_image);
        mOfficialName = new DetailsLabelValueLayout(findViewById(R.id.official_name), R.string.official_name_label);
        mRegion = new DetailsLabelValueLayout(findViewById(R.id.region), R.string.region_label);
        mSubregion = new DetailsLabelValueLayout(findViewById(R.id.subregion), R.string.subregion_label);
        mCapitol = new DetailsLabelValueLayout(findViewById(R.id.capitol), R.string.capitol_label);
        mPopulation = new DetailsLabelValueLayout(findViewById(R.id.population), R.string.population_label);
        mAreaKm = new DetailsLabelValueLayout(findViewById(R.id.area), R.string.area_label);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment tempFragment = fragmentManager.findFragmentByTag(FetchCountryAsyncTaskFragment.TAG);
        if (tempFragment instanceof FetchCountryAsyncTaskFragment) {
            mFetchCountryFragment = (FetchCountryAsyncTaskFragment) tempFragment;
            mLoadingLayout.setVisibility(View.VISIBLE);
            if (mFetchCountryFragment.isComplete()) {
                CountryRecord result = mFetchCountryFragment.getResult();
                setCountryDetails(result);
            } else {
                mFetchCountryFragment.fetchCountry(this, mCountryName);
            }
        } else {
            CountryRecord result = CountryDao.getInstance().getCountryByName(mCountryName);
            if (result != null) {
                setCountryDetails(result);
            } else {
                mLoadingLayout.setVisibility(View.VISIBLE);
                mFetchCountryFragment = new FetchCountryAsyncTaskFragment();
                mFetchCountryFragment.setRetainInstance(true);
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(mFetchCountryFragment, FetchCountryAsyncTaskFragment.TAG);
                transaction.commit();
                mFetchCountryFragment.fetchCountry(this, mCountryName);
            }
        }
    }

    @Override
    public void onCountryFetched(FetchCountryAsyncTaskFragment fetchCountryFragment, String countryName, CountryRecord result) {
        if (fetchCountryFragment == mFetchCountryFragment && mCountryName.equals(countryName)) {
            setCountryDetails(result);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mFetchCountryFragment != null) {
            mFetchCountryFragment.unregisterCallback(this);
            mFetchCountryFragment = null;
        }
    }

    private void setCountryDetails(CountryRecord details) {
        if (details == null) {
            //TODO: Error screen?
            return;
        }
        mLoadingLayout.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(details.getName())) {
            mTitle.setText(details.getName());
        } else {
            mTitle.setText(mCountryName);
        }
        Picasso picasso = PicassoFactory.get(this);
        if (!TextUtils.isEmpty(details.getFlagImgUrl())) {
            RequestCreator request = picasso.load(details.getFlagImgUrl());
            request.resizeDimen(R.dimen.country_details_image_size, R.dimen.country_details_image_size).centerInside().into(mFlagImage);
        } else {
            mFlagImage.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(details.getCoatOfArmsImgUrl())) {
            RequestCreator request = picasso.load(details.getCoatOfArmsImgUrl());
            request.resizeDimen(R.dimen.country_details_image_size, R.dimen.country_details_image_size).centerInside().into(mCoatOfArmsImage);
        } else {
            mCoatOfArmsImage.setVisibility(View.GONE);
        }
        NumberFormat numberFormat = NumberFormat.getIntegerInstance();
        if (details.getPopulation() != null) {
            mPopulation.setValueText(numberFormat.format(details.getPopulation()));
        }
        if (details.getAreaSqKm() != null) {
            mAreaKm.setValueText(numberFormat.format(details.getAreaSqKm()));
        }
        if (!TextUtils.isEmpty(details.getOfficialName())) {
            mOfficialName.setValueText(details.getOfficialName());
        }
        if (!TextUtils.isEmpty(details.getRegion())) {
            mRegion.setValueText(details.getRegion());
        }
        if (!TextUtils.isEmpty(details.getSubRegion())) {
            mSubregion.setValueText(details.getSubRegion());
        }
        if (!TextUtils.isEmpty(details.getCapitol())) {
            mCapitol.setValueText(details.getCapitol());
        }

    }
}
