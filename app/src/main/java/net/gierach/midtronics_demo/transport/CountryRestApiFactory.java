package net.gierach.midtronics_demo.transport;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CountryRestApiFactory {

    private static ICountryRestApi sInstance;

    public static ICountryRestApi getCountryRestApi() {
        if (sInstance == null) {
            Retrofit.Builder builder = new Retrofit.Builder();
            builder.baseUrl("https://restcountries.com/");
            builder.addConverterFactory(GsonConverterFactory.create());

            Retrofit retrofit = builder.build();
            sInstance = retrofit.create(ICountryRestApi.class);
        }

        return sInstance;
    }
}
