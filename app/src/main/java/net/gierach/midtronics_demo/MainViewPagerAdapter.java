package net.gierach.midtronics_demo;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class MainViewPagerAdapter extends FragmentStatePagerAdapter {

    private static final int[] PAGE_TITLES = {
            R.string.countries_tab,
            R.string.profile_tab
    };

    private static final String[] PAGE_CLASSES = {
            CountryListFragment.class.getCanonicalName(),
            ProfileFragment.class.getCanonicalName(),
    };

    private final FragmentManager mFragmentManager;
    private final Context mContext;

    public MainViewPagerAdapter(Context context, @NonNull FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mContext = context;
        mFragmentManager = fm;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mFragmentManager.getFragmentFactory().instantiate(getClass().getClassLoader(),
                PAGE_CLASSES[position]);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getString(PAGE_TITLES[position]);
    }

    @Override
    public int getCount() {
        return PAGE_TITLES.length;
    }
}
