package net.gierach.midtronics_demo;

import android.content.Context;

import com.squareup.picasso.Picasso;

public class PicassoFactory {

    public static Picasso get(Context context) {
        Picasso result = Picasso.get();
        if (result == null) {
            Picasso.Builder picassoBuilder = new Picasso.Builder(context.getApplicationContext());
            result = picassoBuilder.build();
            Picasso.setSingletonInstance(result);
        }

        return result;
    }
}
