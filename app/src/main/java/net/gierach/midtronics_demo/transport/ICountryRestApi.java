package net.gierach.midtronics_demo.transport;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ICountryRestApi {

    @GET("v3.1/name/{countryName}")
    Call<List<RestCountry>> getCountryByName(@Path("countryName") String countryName);
}
