package net.gierach.midtronics_demo.transport;

import java.util.List;

public class RestCountry {

    public RestCountryName name;
    public List<String> capital;
    public String region;
    public String subregion;
    public Long population;
    public Long area;
    public RestCountryImages flags;
    public RestCountryImages coatOfArms;
}
