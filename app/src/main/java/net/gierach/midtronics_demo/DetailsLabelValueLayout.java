package net.gierach.midtronics_demo;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.StringRes;

public class DetailsLabelValueLayout {

    private final TextView value;

    public DetailsLabelValueLayout(View layoutView, @StringRes int labelResId) {
        TextView label = layoutView.findViewById(R.id.detail_label);
        label.setText(labelResId);
        value = layoutView.findViewById(R.id.detail_value);
    }

    public void setValueText(CharSequence text) {
        this.value.setText(text);
    }
}
