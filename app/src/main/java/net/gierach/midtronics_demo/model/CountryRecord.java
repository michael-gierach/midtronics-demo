package net.gierach.midtronics_demo.model;

import net.gierach.midtronics_demo.transport.RestCountry;

public class CountryRecord {
    private final String requestName;
    private final String name;
    private final String officialName;
    private final String capitol;
    private final String region;
    private final String subRegion;
    private final Long population;
    private final Long areaSqKm;
    private final String flagImgUrl;
    private final String coatOfArmsImgUrl;
    private final long timeStamp;

    public CountryRecord(String requestName, RestCountry restCountry) {
        this.timeStamp = System.currentTimeMillis();
        this.requestName = requestName;
        if (restCountry.name != null) {
            this.name = restCountry.name.common;
            this.officialName = restCountry.name.official;
        } else {
            this.name = null;
            this.officialName = null;
        }
        if (restCountry.capital != null && restCountry.capital.size() > 0) {
            this.capitol = restCountry.capital.get(0);
        } else {
            this.capitol = null;
        }
        this.region = restCountry.region;
        this.subRegion = restCountry.subregion;
        this.population = restCountry.population;
        this.areaSqKm = restCountry.area;
        if (restCountry.flags != null) {
            flagImgUrl = restCountry.flags.png;
        } else {
            flagImgUrl = null;
        }
        if (restCountry.coatOfArms != null) {
            coatOfArmsImgUrl = restCountry.coatOfArms.png;
        } else {
            coatOfArmsImgUrl = null;
        }
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getRequestName() {
        return requestName;
    }

    public String getCapitol() {
        return capitol;
    }

    public String getName() {
        return name;
    }

    public String getOfficialName() {
        return officialName;
    }

    public String getRegion() {
        return region;
    }

    public String getSubRegion() {
        return subRegion;
    }

    public Long getPopulation() {
        return population;
    }

    public Long getAreaSqKm() {
        return areaSqKm;
    }

    public String getFlagImgUrl() {
        return flagImgUrl;
    }

    public String getCoatOfArmsImgUrl() {
        return coatOfArmsImgUrl;
    }
}
