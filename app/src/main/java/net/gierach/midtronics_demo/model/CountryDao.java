package net.gierach.midtronics_demo.model;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class CountryDao {

    private static class InstanceHolder {
        static final CountryDao sInstance = new CountryDao();
    }

    public static CountryDao getInstance() {
        return InstanceHolder.sInstance;
    }

    private static final long COUNTRY_TTL = TimeUnit.MINUTES.toMillis(15);

    private final HashMap<String, CountryRecord> mCache = new HashMap<>();

    public synchronized CountryRecord getCountryByName(String countryName) {
        CountryRecord result = mCache.get(countryName);
        if (result != null) {
            if (result.getTimeStamp() + COUNTRY_TTL < System.currentTimeMillis()) {
                result = null;
            }
        }

        return result;
    }

    public synchronized void putCountryRecord(CountryRecord countryRecord) {
        mCache.put(countryRecord.getRequestName(), countryRecord);
    }
}
